### بالعربية:

#### السكربت لتحسين وتحديث النظام وإضافة مستودع Flathub

1. **تحديث النظام:**
   يُحدث قوائم الحزم ويقوم بتحسين النظام.
   
2. **تنظيف الملفات المؤقتة:**
   يقوم بحذف الملفات المؤقتة لتوفير مساحة على القرص.

3. **إضافة مستودع Flathub:**
   يتحقق من وجود مستودع Flathub ويضيفه إذا لم يكن موجودًا.

4. **ترقية Flatpak:**
   يُحدث الحزم المثبتة عبر Flatpak.

#### التنفيذ:
يمكن تنفيذ هذا السكربت على سطر الأوامر.

---

### In English:

#### Script to Improve System, Update, and Add Flathub Repository

1. **Update System:**
   Updates package lists and improves the system.
   
2. **Clean Temporary Files:**
   Deletes temporary files to free up disk space.
   
3. **Add Flathub Repository:**
   Checks for the existence of the Flathub repository and adds it if not present.
   
4. **Upgrade Flatpak:**
   Updates installed Flatpak packages.

#### Execution:
This script can be executed in the command line.
