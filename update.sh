#!/bin/bash
# A sample Bash script by FAHAD ALGHATHBAR

# تحديث النظام
echo "جارٍ تحديث قوائم الحزم..."
sudo apt-get update

# تحسين النظام
echo "جارٍ تحسين النظام..."
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y

# تنظيف الملفات المؤقتة
echo "جارٍ تنظيف الملفات المؤقتة..."
sudo apt-get autoclean
sudo apt-get clean

# إضافة مستودع Flathub إذا لم يكن مضافًا
echo "جارٍ التحقق من مستودع Flathub..."
if ! flatpak remotes | grep -q flathub; then
    echo "إضافة مستودع Flathub..."
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
else
    echo "مستودع Flathub مضاف بالفعل."
fi

# ترقية Flatpak
echo "جارٍ ترقية الحزم عبر Flatpak..."
sudo flatpak update -y

echo "انتهت عملية التحديث والترقية للنظام وFlatpak."
